"""
COMPONENTES: 
    Elias de Jesus Castro Sarges
    Aleff Jhones
    Rodrigo Ferreira
"""

from tkinter import *
import tkinter.font as tkFont


# tel   
screen = Tk()
screen.title('Menu')

screen.resizable(0,0)
screen.geometry("300x400")

#container 
frame = Frame(screen)
frame.pack()

#fonte 1
font1 = tkFont.Font(family='Helvetica',
        size=16, weight='bold')
#fonte 2
font2 = tkFont.Font(family='Helvetica',
        size= 15)

#scrollbar
scrollbar =  Scrollbar(frame, orient=VERTICAL)

contacts = Listbox(frame, yscrollcommand=scrollbar.set, width = 30, height = 12, 
                activestyle=NONE, selectmode=EXTENDED, bd="0", font=font1, selectbackground="lightgreen")

# data = [["Elias", 1234, "Teste", "Teste"],["Elias", 1234, "Teste", "Teste"],["Elias", 1234, "Teste", "Teste"],["Elias", 1234, "Teste", "Teste"],["Elias", 1234, "Teste", "Teste"],["Elias", 1234, "Teste", "Teste"],["Elias", 1234, "Teste", "Teste"],["Elias", 1234, "Teste", "Teste"],["Elias", 1234, "Teste", "Teste"],["Elias", 1234, "Teste", "Teste"],["Elias", 1234, "Teste", "Teste"],["Elias", 1234, "Teste", "Teste"],["Elias", 1234, "Teste", "Teste"],["Elias", 1234, "Teste", "Teste"],["Elias", 1234, "Teste", "Teste"],["Elias", 1234, "Teste", "Teste"],["Elias", 1234, "Teste", "Teste"],["Elias", 1234, "Teste", "Teste"],["Elias", 1234, "Teste", "Teste"],["Elias", 1234, "Teste", "Teste"],["Elias", 1234, "Teste", "Teste"],["Elias", 1234, "Teste", "Teste"]]

# for i in range(len(data)):
#     contacts.insert(END, data[i][0])

data = []

#configuração do scroll
scrollbar.config(command=contacts.yview)
scrollbar.pack(side=RIGHT, fill=Y)#desenha o scroll
contacts.pack(side=LEFT, padx=10, pady=15, fill="both")

def add_contact():
    screen = Toplevel()
    screen.grab_set()
    screen.focus()

    screen.geometry("300x400")
    screen.resizable(0,0)

    #cria a caixa de texto 
    lb_name = Label(screen, text = "Nome", pady=5)
    name = Entry(screen, width = 45)

    lb_name.pack()
    name.pack()

    name.focus()

    lb_number = Label(screen, text = "Numero", pady=5)
    number = Entry(screen, width = 45)

    lb_number.pack()
    number.pack()

    lb_email = Label(screen, text = "E-Mail", pady=5)
    email = Entry(screen, width = 45)

    lb_email.pack()
    email.pack()

    lb_adress = Label(screen, text = "Endereço", pady=5)
    adress = Entry(screen, width = 45)
    
    lb_adress.pack()
    adress.pack()

    def save():
        data.append([name.get(), number.get(), email.get(), adress.get()])
        contacts.insert(END, name.get())
        screen.destroy()

    save_btn = Button(screen, text="Salvar", command = save).place(x=125, y=325)
add_btn = Button(screen, text="Adicionar", command = add_contact, width=10).place(x= 190, y= 325)

def remove_contact():
    contacts.delete(ANCHOR)
remove_contact = Button(screen, text = "Remover ", command= remove_contact, width=10).place(x= 190, y= 355)

def search_contact():
    screen = Toplevel()
    screen.resizable(0,0)
    screen.grab_set()
    screen.focus()


    input_search = Entry(screen, width = 50)
    input_search.pack()
    input_search.focus()


    def search():
        values = contacts.get(0, END)
        value = input_search.get()
        for i in range(len(values)):
            if value == values[i]:
                contacts.selection_set(i)
                contacts.see(i)
                screen.destroy()

                new_screen = Toplevel()
                new_screen.geometry("200x300")
                new_screen.resizable(0,0)
                new_screen.grab_set()
                new_screen.focus()

                #nome
                lb_name = Label(new_screen, text = "Nome", font = font1, bg="lightgreen")
                data_name = Label(new_screen, text= data[i][0], font = font2)
                lb_name.pack()
                data_name.pack()

                
                #numero
                lb_number = Label(new_screen, text = "Numero", font = font1, bg="lightgreen")
                data_number = Label(new_screen, text= data[i][1], font = font2)
                lb_number.pack()
                data_number.pack()

                #email
                lb_email = Label(new_screen, text = "E-Mail", font = font1, bg="lightgreen")
                data_email = Label(new_screen, text= data[i][2], font = font2)
                lb_email.pack()
                data_email.pack()

                #endereço
                lb_adress = Label(new_screen, text = "Endereço", font = font1, bg="lightgreen")
                data_adress = Label(new_screen, text= data[i][3], font = font2)
                lb_adress.pack()
                data_adress.pack()
                            
    search_btn = Button(screen, text="procurar", command = search).pack()
search_contact = Button(screen, text = "Procurar", command = search_contact, width=10).place(x= 15, y= 355)

def edit_contact():
    #cria a janela
    screen = Toplevel()
    screen.grab_set()
    screen.focus()

    #configurações da janela
    screen.geometry("300x400")
    screen.resizable(0,0)


    selection_index = contacts.index(ANCHOR)

    #Labels e Inputs

    #nome
    lb_name = Label(screen, text = "Nome", pady=5)
    name = Entry(screen, width = 45)
    name.insert(0, data[selection_index][0])
    lb_name.pack()
    name.pack()
    name.focus()

    
    #numero
    lb_number = Label(screen, text = "Numero", pady=5)
    number = Entry(screen, width = 45)
    number.insert(0, data[selection_index][1])
    lb_number.pack()
    number.pack()

    #email
    lb_email = Label(screen, text = "E-Mail", pady=5)
    email = Entry(screen, width = 45)
    email.insert(0, data[selection_index][2])
    lb_email.pack()
    email.pack()

    #endereço
    lb_adress = Label(screen, text = "Endereço", pady=5)
    adress = Entry(screen, width = 45)
    adress.insert(0, data[selection_index][3])
    lb_adress.pack()
    adress.pack()

    def save():
        data[selection_index][0] = name.get()
        data[selection_index][1] = number.get()
        data[selection_index][2] = email.get()
        data[selection_index][3] = adress.get()

        contacts.delete(ANCHOR)
        contacts.insert(selection_index, data[selection_index][0])
        screen.destroy()

    save_btn = Button(screen, text="Salvar", command = save).place(x=125, y=325)
ed_contact = Button(screen, text = "Editar", command = edit_contact, width=10).place(x= 15, y= 325)

screen.mainloop()